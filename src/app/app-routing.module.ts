import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomePageComponent} from './pages/home-page/home-page.component';
import { OilProductPageComponent} from './pages/oil-product-page/oil-product-page.component';
import { SerumProductPageComponent} from './pages/serum-product-page/serum-product-page.component';
import { ContactPageComponent} from './pages/contact-page/contact-page.component';
import { ShopComponent } from './pages/shop/shop.component'
 




const routes: Routes = [
  {path: '', component: HomePageComponent},
  {path: 'products/restoring-oil', component: OilProductPageComponent},
  {path: 'products/restoring-serum', component: SerumProductPageComponent},
  {path: 'contact-us', component: ContactPageComponent},
  {path: 'shop', component: ShopComponent}


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
