import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OilProductPageComponent } from './oil-product-page.component';

describe('OilProductPageComponent', () => {
  let component: OilProductPageComponent;
  let fixture: ComponentFixture<OilProductPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OilProductPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OilProductPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
