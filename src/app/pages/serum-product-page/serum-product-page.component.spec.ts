import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SerumProductPageComponent } from './serum-product-page.component';

describe('SerumProductPageComponent', () => {
  let component: SerumProductPageComponent;
  let fixture: ComponentFixture<SerumProductPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SerumProductPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SerumProductPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
