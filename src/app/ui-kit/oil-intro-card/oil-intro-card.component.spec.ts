import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OilIntroCardComponent } from './oil-intro-card.component';

describe('OilIntroCardComponent', () => {
  let component: OilIntroCardComponent;
  let fixture: ComponentFixture<OilIntroCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OilIntroCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OilIntroCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
