import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FindStoreCardComponent } from './find-store-card.component';

describe('FindStoreCardComponent', () => {
  let component: FindStoreCardComponent;
  let fixture: ComponentFixture<FindStoreCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FindStoreCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FindStoreCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
