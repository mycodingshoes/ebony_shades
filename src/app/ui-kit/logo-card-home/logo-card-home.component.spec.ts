import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogoCardHomeComponent } from './logo-card-home.component';

describe('LogoCardHomeComponent', () => {
  let component: LogoCardHomeComponent;
  let fixture: ComponentFixture<LogoCardHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogoCardHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogoCardHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
