import { TestBed } from '@angular/core/testing';

import { FindStoreModalService } from './find-store-modal.service';

describe('FindStoreModalService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FindStoreModalService = TestBed.get(FindStoreModalService);
    expect(service).toBeTruthy();
  });
});
