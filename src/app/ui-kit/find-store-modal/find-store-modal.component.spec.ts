import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FindStoreModalComponent } from './find-store-modal.component';

describe('FindStoreModalComponent', () => {
  let component: FindStoreModalComponent;
  let fixture: ComponentFixture<FindStoreModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FindStoreModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FindStoreModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
