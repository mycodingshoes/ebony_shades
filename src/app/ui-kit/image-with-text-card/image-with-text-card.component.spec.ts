import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageWithTextCardComponent } from './image-with-text-card.component';

describe('ImageWithTextCardComponent', () => {
  let component: ImageWithTextCardComponent;
  let fixture: ComponentFixture<ImageWithTextCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImageWithTextCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageWithTextCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
