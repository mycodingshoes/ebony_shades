import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SerumIntroCardComponent } from './serum-intro-card.component';

describe('SerumIntroCardComponent', () => {
  let component: SerumIntroCardComponent;
  let fixture: ComponentFixture<SerumIntroCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SerumIntroCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SerumIntroCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
