declare var require : any;
import { Component, OnInit } from '@angular/core';
// import { WindowService } from 'src/app/service/window.service';


@Component({
  selector: 'app-product-cards',
  templateUrl: './product-cards.component.html',
  styleUrls: ['./product-cards.component.scss']
})
export class ProductCardsComponent implements OnInit {
  defaultImage = '/assets/images/loading.gif';
  oilImage = '/assets/images/oil.png';
  serumImage = '/assets/images/serum.png';
  constructor() { }

  ngOnInit() {
  }

}
