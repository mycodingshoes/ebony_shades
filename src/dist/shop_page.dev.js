"use strict";

/*product1*/

/*<![CDATA[*/
(function () {
  var scriptURL = 'https://sdks.shopifycdn.com/buy-button/latest/buy-button-storefront.min.js';

  if (window.ShopifyBuy) {
    if (window.ShopifyBuy.UI) {
      ShopifyBuyInit();
    } else {
      loadScript();
    }
  } else {
    loadScript();
  }

  function loadScript() {
    var script = document.createElement('script');
    script.async = true;
    script.src = scriptURL;
    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(script);
    script.onload = ShopifyBuyInit;
  }

  function ShopifyBuyInit() {
    var client = ShopifyBuy.buildClient({
      domain: 'ebonyshades.myshopify.com',
      storefrontAccessToken: '7dc4d640091f5fb64300fed6cf54a012'
    });
    ShopifyBuy.UI.onReady(client).then(function (ui) {
      ui.createComponent('product', {
        id: '6150693716157',
        node: document.getElementById('product-component-1610208083078'),
        moneyFormat: 'R%20%7B%7Bamount%7D%7D',
        options: {
          "product": {
            "styles": {
              "product": {
                "@media (min-width: 601px)": {
                  "max-width": "100%",
                  "margin-left": "0",
                  "margin-bottom": "50px"
                },
                "text-align": "left"
              },
              "title": {
                "font-size": "26px"
              },
              "button": {
                ":hover": {
                  "background-color": "#2758a0"
                },
                "background-color": "#17345e",
                ":focus": {
                  "background-color": "#2758a0"
                }
              },
              "price": {
                "font-size": "18px"
              },
              "compareAt": {
                "font-size": "15.299999999999999px"
              },
              "unitPrice": {
                "font-size": "15.299999999999999px"
              }
            },
            "layout": "horizontal",
            "contents": {
              "img": false,
              "imgWithCarousel": true,
              "description": true
            },
            "width": "100%",
            "text": {
              "button": "Add to cart"
            }
          },
          "productSet": {
            "styles": {
              "products": {
                "@media (min-width: 601px)": {
                  "margin-left": "-20px"
                }
              }
            }
          },
          "modalProduct": {
            "contents": {
              "img": false,
              "imgWithCarousel": true,
              "button": false,
              "buttonWithQuantity": true
            },
            "styles": {
              "product": {
                "@media (min-width: 601px)": {
                  "max-width": "100%",
                  "margin-left": "0px",
                  "margin-bottom": "0px"
                }
              },
              "button": {
                ":hover": {
                  "background-color": "#2758a0"
                },
                "background-color": "#17345e",
                ":focus": {
                  "background-color": "#2758a0"
                }
              }
            },
            "text": {
              "button": "Add to cart"
            }
          },
          "cart": {
            "styles": {
              "button": {
                ":hover": {
                  "background-color": "#2758a0"
                },
                "background-color": "#17345e",
                ":focus": {
                  "background-color": "#2758a0"
                }
              }
            },
            "text": {
              "total": "Subtotal",
              "button": "Checkout"
            }
          },
          "toggle": {
            "styles": {
              "toggle": {
                "background-color": "#17345e",
                ":hover": {
                  "background-color": "#2758a0"
                },
                ":focus": {
                  "background-color": "#2758a0"
                }
              }
            }
          }
        }
      });
    });
  }
})();
/*]]>*/

/*product2*/

/*<![CDATA[*/


(function () {
  var scriptURL = 'https://sdks.shopifycdn.com/buy-button/latest/buy-button-storefront.min.js';

  if (window.ShopifyBuy) {
    if (window.ShopifyBuy.UI) {
      ShopifyBuyInit();
    } else {
      loadScript();
    }
  } else {
    loadScript();
  }

  function loadScript() {
    var script = document.createElement('script');
    script.async = true;
    script.src = scriptURL;
    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(script);
    script.onload = ShopifyBuyInit;
  }

  function ShopifyBuyInit() {
    var client = ShopifyBuy.buildClient({
      domain: 'ebonyshades.myshopify.com',
      storefrontAccessToken: '7dc4d640091f5fb64300fed6cf54a012'
    });
    ShopifyBuy.UI.onReady(client).then(function (ui) {
      ui.createComponent('product', {
        id: '6150697222333',
        node: document.getElementById('product-component-1610208244077'),
        moneyFormat: 'R%20%7B%7Bamount%7D%7D',
        options: {
          "product": {
            "styles": {
              "product": {
                "@media (min-width: 601px)": {
                  "max-width": "100%",
                  "margin-left": "0",
                  "margin-bottom": "50px"
                },
                "text-align": "left"
              },
              "title": {
                "font-size": "26px"
              },
              "button": {
                ":hover": {
                  "background-color": "#2758a0"
                },
                "background-color": "#17345e",
                ":focus": {
                  "background-color": "#2758a0"
                }
              },
              "price": {
                "font-size": "18px"
              },
              "compareAt": {
                "font-size": "15.299999999999999px"
              },
              "unitPrice": {
                "font-size": "15.299999999999999px"
              }
            },
            "layout": "horizontal",
            "contents": {
              "img": false,
              "imgWithCarousel": true,
              "description": true
            },
            "width": "100%",
            "text": {
              "button": "Add to cart"
            }
          },
          "productSet": {
            "styles": {
              "products": {
                "@media (min-width: 601px)": {
                  "margin-left": "-20px"
                }
              }
            }
          },
          "modalProduct": {
            "contents": {
              "img": false,
              "imgWithCarousel": true,
              "button": false,
              "buttonWithQuantity": true
            },
            "styles": {
              "product": {
                "@media (min-width: 601px)": {
                  "max-width": "100%",
                  "margin-left": "0px",
                  "margin-bottom": "0px"
                }
              },
              "button": {
                ":hover": {
                  "background-color": "#2758a0"
                },
                "background-color": "#17345e",
                ":focus": {
                  "background-color": "#2758a0"
                }
              }
            },
            "text": {
              "button": "Add to cart"
            }
          },
          "cart": {
            "styles": {
              "button": {
                ":hover": {
                  "background-color": "#2758a0"
                },
                "background-color": "#17345e",
                ":focus": {
                  "background-color": "#2758a0"
                }
              }
            },
            "text": {
              "total": "Subtotal",
              "button": "Checkout"
            }
          },
          "toggle": {
            "styles": {
              "toggle": {
                "background-color": "#17345e",
                ":hover": {
                  "background-color": "#2758a0"
                },
                ":focus": {
                  "background-color": "#2758a0"
                }
              }
            }
          }
        }
      });
    });
  }
})();
/*]]>*/